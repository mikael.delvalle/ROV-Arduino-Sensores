/* Sensors and code by BlueRobotics. 
 * Sensor code modified by Christian Colon Lopez 
 * Pressure Sensor & Temperature Sensor 
 * Project: ROV
 * University of Puerto Rico Mayaguez Campus 
 */

/*-----( Import needed libraries )-----*/
#include <Wire.h>
#include <MS5837.h>
#include <TSYS01.h>
/*-----( Declare Constants and Pin Numbers )-----*/
/*-----( Declare objects )-----*/
MS5837 sensor1;
TSYS01 sensor2;
/*-----( Declare Variables )-----*/


void setup()   /****** SETUP: RUNS ONCE ******/
{
  Serial.begin(9600);
//  
//  Serial.println("Starting");
  
  Wire.begin();

  sensor1.init();

  sensor2.init();
  
  sensor1.setFluidDensity(997); // kg/m^3 (freshwater, 1029 for seawater)

}//--(end setup )---


void loop()   /****** LOOP: RUNS CONSTANTLY ******/
{
  sensor1.read();
  sensor1.pressure();
  sensor1.depth();
  sensor1.altitude();
  delay(1000);

  sensor2.read();
  sensor2.temperature();
  delay(1000);
  

/*
Serial.print("Depth: "); 
  Serial.print(sensor1.depth()); 
  Serial.println(" m");

  Serial.print("Altitude: "); 
  Serial.print(sensor1.altitude()); 
  Serial.println(" m above mean sea level");

  delay(1000);

  sensor2.read();

  Serial.print("Temperature: ");  
  Serial.print(sensor2.temperature());  
  Serial.println(" deg C");

  delay(1000);
  
*/
}//--(end main loop )---

/*-----( Declare User-written Functions )-----*/


void sentValueToArduinoMaster(){
  
  
  
  
  
  
  }


//*********( THE END )***********